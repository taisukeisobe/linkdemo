-- Table: public.dropzone
DROP TABLE public.dropzone;
CREATE TABLE public.dropzone
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    dropzone character varying(255) COLLATE pg_catalog."default",
    img character varying(255) COLLATE pg_catalog."default",
    linkid character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT dropzone_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.dropzone
    OWNER to postgres;
-- Table: public.imglist
DROP TABLE public.imglist;
CREATE TABLE public.imglist
(
    imgid character varying(255) COLLATE pg_catalog."default" NOT NULL,
    linkid character varying(255) COLLATE pg_catalog."default",
    width integer,
    height integer,
    CONSTRAINT imglist_pkey PRIMARY KEY (imgid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.imglist
    OWNER to postgres;
-- Table: public.linkidlicensemanager
DROP TABLE public.linkidlicensemanager;
CREATE TABLE public.linkidlicensemanager
(
    linkid character varying COLLATE pg_catalog."default" NOT NULL,
    userid integer,
    status boolean,
    name character varying COLLATE pg_catalog."default",
    "time" timestamp without time zone,
    CONSTRAINT linkidlicensemanager_pkey PRIMARY KEY (linkid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.linkidlicensemanager
    OWNER to postgres;
-- Table: public.member
 DROP TABLE public.member;
CREATE TABLE public.member
(
    username character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    userid integer NOT NULL,
    displyname character varying COLLATE pg_catalog."default",
    CONSTRAINT member_pkey PRIMARY KEY (userid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.member
    OWNER to postgres;
-- Table: public.popupitem
 DROP TABLE public.popupitem;
CREATE TABLE public.popupitem
(
    color character varying COLLATE pg_catalog."default",
    top integer,
    leftposition integer,
    linkid character varying COLLATE pg_catalog."default",
    id character varying COLLATE pg_catalog."default" NOT NULL,
    size character varying COLLATE pg_catalog."default",
    target character varying COLLATE pg_catalog."default",
    itemid character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT popupitem_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.popupitem
    OWNER to postgres;
-- Table: public.stickyitem
 DROP TABLE public.stickyitem;
CREATE TABLE public.stickyitem
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    linkid character varying COLLATE pg_catalog."default",
    itemid character varying COLLATE pg_catalog."default",
    color character varying COLLATE pg_catalog."default",
    height integer,
    width integer,
    top integer,
    leftposition integer,
    text character varying COLLATE pg_catalog."default",
    CONSTRAINT stickyitem_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.stickyitem
    OWNER to postgres;
-- Table: public.tmpmember
DROP TABLE public.tmpmember;
CREATE TABLE public.tmpmember
(
    userid integer NOT NULL,
    username character varying COLLATE pg_catalog."default",
    password character varying COLLATE pg_catalog."default",
    displyname character varying COLLATE pg_catalog."default",
    validation character varying COLLATE pg_catalog."default",
    CONSTRAINT "tmp-member_pkey" PRIMARY KEY (userid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public.tmpmember
    OWNER to postgres;