package com.linkdemo.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.linkdemo.domain.bean.repository.MemberRepository;
import com.linkdemo.domain.bean.table.Member;

@Service
public class MemberServiceImpl implements UserDetailsService {
	private MemberRepository memberRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (StringUtils.isEmpty(username)) {
			System.out.println("notfound");
			throw new UsernameNotFoundException("");
		}

		Member memberEntity = memberRepository.findByUsername(username);
		if (memberEntity == null) {
			throw new UsernameNotFoundException("");
		}


		System.out.println("userpas+" + memberEntity.getPassword());
		System.out.println("username+" + memberEntity.getUsername());


		return memberEntity;
	}

	@Autowired
	public void setMemberRepository(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}
}