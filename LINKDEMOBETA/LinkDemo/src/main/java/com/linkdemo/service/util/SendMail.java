package com.linkdemo.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
@Component
public class SendMail {
	@Autowired
	JavaMailSender mailSender;


	public void sendMail(String to,String from,String titile,String content){

	SimpleMailMessage msg = new SimpleMailMessage();
	System.out.println(to);

    msg.setFrom(from);
    msg.setTo(to);
    msg.setSubject(titile);//タイトルの設定
    msg.setText(content); //本文の設定
    mailSender.send(msg);
	}
}
