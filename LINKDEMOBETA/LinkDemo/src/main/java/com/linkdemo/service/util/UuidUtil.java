package com.linkdemo.service.util;

import java.util.UUID;

public class UuidUtil {


	private UuidUtil(){};

	public static String generateUUID(){
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}



}
