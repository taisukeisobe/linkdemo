package com.linkdemo.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.linkdemo.domain.bean.DropZoneWithImgBean;
import com.linkdemo.domain.bean.repository.LinkidLicenseManagerRepository;
import com.linkdemo.domain.bean.table.DropZone;
import com.linkdemo.domain.bean.table.ImgList;
import com.linkdemo.domain.bean.table.LinkidLicenseManager;
@Controller
public class AddImgToDropZoneList {


	@Autowired
	LinkidLicenseManagerRepository linkidLicenseManagerRepository;

	public List<DropZoneWithImgBean> dropzoneWithImglist(List<DropZone> dropzoneList, List<ImgList> imgList, LinkidLicenseManager llm)
			throws IOException {

		String imgfolder = "/opt/LinkDemo/img/";
	//	String imgfolder = "/Users/isobetaisuke/Desktop/bitbucketLinkdemo/LINKDEMOBETA/LinkDemo/img/";
		String user_folder = null;

		//String tempimgfolder = "/opt/LinkDemo/img/"
		//		+ dropzoneList.get(0).getLinkid();

	//	String imgFolder = null;



		int userid = llm.getUserid();
		String linkid = llm.getLinkid();
				//lim.getUserid();

		if(userid == 0){
			user_folder = imgfolder + "tmp" + "/" + linkid;

		}else{
			user_folder = imgfolder + userid + "/" + linkid;
		}


		List<DropZoneWithImgBean> dropzoneWithImglist = new ArrayList<DropZoneWithImgBean>();
		StringBuffer data ;
		InputStream fileStream;

		int leng = dropzoneList.size();
		int lengImg = imgList.size();
		int width = 0;
		int height = 0;

		for (int i = 0; i < leng; i++) {


			try {

				if (dropzoneList.get(i).getImg() != null && !(dropzoneList.get(i).getImg().equals("0"))) {

					for (int j = 0; j < lengImg; j++) {

						if (imgList.get(j).getImgid().equals(dropzoneList.get(i).getImg())) {

							width = imgList.get(j).getWidth();
							height = imgList.get(j).getHeight();
						}
					}

					fileStream = new FileInputStream(user_folder + "/" + dropzoneList.get(i).getImg() + ".png");

					ByteArrayOutputStream os = new ByteArrayOutputStream();
					byte[] indata = new byte[10240 * 16];
					int siz;

					try {
						while ((siz = fileStream.read(indata, 0, indata.length)) > 0) {
							os.write(indata, 0, siz);
						}
					} catch (Exception e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
						System.out.print("failed to read byte stream");
						fileStream.close();

					}

					String base64 = new String(Base64.encodeBase64(os.toByteArray()), "ASCII");

					// System.out.print(base64);

					data = new StringBuffer();
					data.append("data:image/png;base64,");
					data.append(base64);
					fileStream.close();

					dropzoneWithImglist.add(new DropZoneWithImgBean(dropzoneList.get(i).getId(),
							dropzoneList.get(i).getLinkid(), dropzoneList.get(i).getDropzone(),
							dropzoneList.get(i).getImg(), data.toString(), width, height));
				} else {
					dropzoneWithImglist.add(new DropZoneWithImgBean(dropzoneList.get(i).getId(),
							dropzoneList.get(i).getLinkid(), dropzoneList.get(i).getDropzone(),
							dropzoneList.get(i).getImg(), "./../../img/default-none.png", 1300, 1000));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return dropzoneWithImglist;

	}

}
