package com.linkdemo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RootController {

	@RequestMapping(value = "/")
	public ModelAndView moveToEdit(ModelAndView mav){

		//session.invalidate();

			mav.setViewName("index");

		return mav;

	}



}
