package com.linkdemo.web;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.linkdemo.service.util.UuidUtil;

@Controller
public class EditMapController {

	@Autowired
	HttpSession session;

		@RequestMapping(value = "/edit/begin")
		public ModelAndView moveToEdit(ModelAndView mav,@ModelAttribute("isRegisterd") String isRegisterd){

			//session.invalidate();
		String linkid = UuidUtil.generateUUID();
				//session.removeAttribute("linkid");
				session.setAttribute("linkid", linkid);

				session.setAttribute("isRegisterd", isRegisterd);


				mav.setViewName("editmap");
				mav.addObject("linkid",linkid);
				//System.out.println("edit::" + linkid);
			return mav;

		}


}

