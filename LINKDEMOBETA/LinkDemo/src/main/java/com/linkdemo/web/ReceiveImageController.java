package com.linkdemo.web;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.linkdemo.domain.bean.repository.ImgListRepository;
import com.linkdemo.domain.bean.table.ImgList;
import com.linkdemo.domain.bean.table.Member;
import com.linkdemo.service.util.UuidUtil;

@RestController
public class ReceiveImageController {

	// 画像をバイト配列で返却できるようにする
	// http://arimodoki.dip.jp/promenade/base64image.html?siteKeyword=Spring-MVC%2CMaven%2CEclipse%2CThymeleaf%2CMyBatis%2CJava%2C%E6%95%A3%E6%AD%A9%E9%81%93%2C%E5%B0%8F%E5%BE%84%2C%E5%B0%8F%E9%81%93%2C%E5%B0%8F%E8%B7%AF&siteDescription=Eclipse+Maven+Spring-MVC+%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%9FWeb%E3%82%B5%E3%82%A4%E3%83%88%E6%A7%8B%E7%AF%89%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E3%83%9B%E3%83%BC%E3%83%A0%E3%83%9A%E3%83%BC%E3%82%B8%E3%81%A7%E3%81%99

	@Autowired
	HttpSession session;

	@Autowired
	ImgListRepository imgListRepository;

	@RequestMapping(value = "/edit/uploadimg", method = RequestMethod.POST)
	@ResponseBody
	public String upload(@RequestParam MultipartFile file, @AuthenticationPrincipal Member member) {

		String isOkay = null;
		String fileid = UuidUtil.generateUUID().substring(0, 5);
		String linkid = (String) session.getAttribute("linkid");
		int width = 0;
		int height = 0;
		int userid = 0;

		String imgfolder = "/opt/LinkDemo/img/";
	//String imgfolder = "/Users/isobetaisuke/Desktop/bitbucketLinkdemo/LINKDEMOBETA/LinkDemo/img/";


		//String imgfile = "/opt/LinkDemo/img/";
		// String tempimgfolder = "/opt/LinkDemo/img/tmp/" + linkid;
		// String tempimgfile = "/opt/LinkDemo/img/tmp/" + linkid + "/" + fileid
		// + ".png";

		// String tempimgserver = "/img/" + filename ;
		// String tempimgfolder =
		// "/Users/isobetaisuke/Desktop/bitbucketLinkdemo/LINKDEMOBETA/LinkDemo/img/tmp/"
		// + linkid;
		// String tempimgfile =
		// "/Users/isobetaisuke/Desktop/bitbucketLinkdemo/LINKDEMOBETA/LinkDemo/img/tmp/"
		// + linkid
		// + "/" + fileid + ".png";

		try {
			BufferedImage image = ImageIO.read(file.getInputStream());
			width = image.getWidth();
			height = image.getHeight();
		} catch (Exception e) {
			e.printStackTrace();
		};

		if (member != null) {
			//for registered
			userid = (int) member.getUserid();

			String user_folder = imgfolder + userid ;
			String user_imgfolder = imgfolder + userid + "/" + linkid;
			String user_imgfile = imgfolder + userid + "/" + linkid+ "/" + fileid + ".png";

			File userfolder = new File(user_folder);
			File folder = new File(user_imgfolder);
			if (!folder.exists()) {
				// System.out.print(folder.exists());
				userfolder.mkdir();
				folder.mkdir();
			}

			try {
				File uploadDir = new File(user_imgfile.toString());
				file.transferTo(uploadDir);
			} catch (IllegalStateException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		}else{
			//for tmp user
			String tmp_img = imgfolder + "tmp";
			String tmp_imgfolder = imgfolder + "tmp" + "/" + linkid;
			String tmp_imgfile = imgfolder + "tmp" + "/" + linkid+ "/" + fileid + ".png";



			File tmp_folder = new File(tmp_img);
			File folder = new File(tmp_imgfolder);
			// System.out.print(!folder.exists());
			if (!folder.exists()) {
				tmp_folder.mkdir();
				folder.mkdir();
			}
			try {
				System.out.print(tmp_imgfile);
				File uploadDir = new File(tmp_imgfile.toString());
				file.transferTo(uploadDir);
			} catch (IllegalStateException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();

			}
		}

		ImgList imglist = new ImgList(fileid, linkid, width, height);
		imgListRepository.saveAndFlush(imglist);

		isOkay = fileid;

		return isOkay;
	}

}
