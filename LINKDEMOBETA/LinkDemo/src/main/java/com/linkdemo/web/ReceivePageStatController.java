package com.linkdemo.web;


import java.sql.Timestamp;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.linkdemo.domain.bean.ItemBean;
import com.linkdemo.domain.bean.repository.DropZoneRepository;
import com.linkdemo.domain.bean.repository.LinkidLicenseManagerRepository;
import com.linkdemo.domain.bean.repository.PopupItemRepository;
import com.linkdemo.domain.bean.repository.StickyItemRepository;
import com.linkdemo.domain.bean.repository.TooltipItemRepository;
import com.linkdemo.domain.bean.table.DropZone;
import com.linkdemo.domain.bean.table.LinkidLicenseManager;
import com.linkdemo.domain.bean.table.Member;
import com.linkdemo.domain.bean.table.PopupItem;
import com.linkdemo.domain.bean.table.StickyItem;
import com.linkdemo.domain.bean.table.TooltipItem;

//@Controller
@RestController
public class ReceivePageStatController {

	@Autowired
	PopupItemRepository popupRepository;
	@Autowired
	StickyItemRepository stickyRepository;
	@Autowired
	DropZoneRepository dropzoneRepository;
	@Autowired
	TooltipItemRepository tooltipRepository;
	@Autowired
	LinkidLicenseManagerRepository linkidLicenseManagerRepository;

	@Autowired
	HttpSession session;

	@RequestMapping(value = "/edit/uploaditemstat", method = RequestMethod.POST)
	public @ResponseBody int getSearchUserProfiles(@RequestBody ItemBean[] items, HttpServletRequest request,@AuthenticationPrincipal Member member, @RequestParam("name") String demoname) {

		//System.out.println(demoname);

		boolean isRegisterd = false;
		int userid = 0;

		if (member !=null)
		{
			isRegisterd = true;
			userid = (int)member.getUserid();

		}else{
			isRegisterd = false;
		};




		String linkid = (String) session.getAttribute("linkid");
		System.out.println(linkid);
		ArrayList<PopupItem> popupList = new ArrayList<PopupItem>();
		ArrayList<StickyItem> stickyList = new ArrayList<StickyItem>();
		ArrayList<TooltipItem> tooltipList = new ArrayList<TooltipItem>();
		ArrayList<DropZone> dropzoneList = new ArrayList<DropZone>();

		int itemLeng = items.length;

		for (int i = 0; i < itemLeng; i++) {
			if (items[i].getType().equals("_popup")) {

				popupList.add(new PopupItem(items[i].getId(), linkid, items[i].getItemid(), items[i].getColor(),
						items[i].getSize(), items[i].getTarget(), items[i].getTop(), items[i].getLeft()));

				// System.out.println(items[i].getTop() + items[i].getLeft());
			} else if (items[i].getType().equals("_sticky")) {

				stickyList.add(new StickyItem(items[i].getId(), linkid, items[i].getItemid(), items[i].getColor(),
						items[i].getHeight(), items[i].getWidth(), items[i].getTop(), items[i].getLeft(),
						items[i].getText()));

			} else if (items[i].getType().equals("_tooltip")) {


				tooltipList.add(new TooltipItem(items[i].getId(), linkid, items[i].getItemid(), items[i].getColor(),
						items[i].getSize(), items[i].getTop(), items[i].getLeft(), items[i].getTitle()));

			} else if (items[i].getType().equals("drop-zone")) {


				dropzoneList.add(new DropZone(items[i].getId(), linkid, items[i].getDropzone(), items[i].getImg()));

			}

		}

		int popuplistLeng = popupList.size();
		int stickylistLeng = stickyList.size();
		int dropzonelistLeng = dropzoneList.size();
		int tooltoplistLeng = tooltipList.size();

		for (int i = 0; i < popuplistLeng; i++) {
			popupRepository.saveAndFlush(popupList.get(i));
		}

		for (int i = 0; i < stickylistLeng; i++) {
			//System.out.println("here" + stickyList.get(i).getId());
			stickyRepository.saveAndFlush(stickyList.get(i));
		}
		for (int i = 0; i < dropzonelistLeng; i++) {
			dropzoneRepository.saveAndFlush(dropzoneList.get(i));
		}
		for (int i = 0; i < tooltoplistLeng; i++) {
			tooltipRepository.saveAndFlush(tooltipList.get(i));
		}

		int pName = items[0].getTop();
		// System.out.println(session.getAttribute("linkid"));

		//LinkidLicenseManager lim = new LinkidLicenseManager(linkid,userid,isRegisterd);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		linkidLicenseManagerRepository.saveAndFlush(new LinkidLicenseManager(linkid,userid,isRegisterd,demoname,timestamp));

		return pName;
	}

}
