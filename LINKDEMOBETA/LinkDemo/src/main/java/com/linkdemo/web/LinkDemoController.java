package com.linkdemo.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.linkdemo.domain.bean.DropZoneWithImgBean;
import com.linkdemo.domain.bean.repository.DropZoneRepository;
import com.linkdemo.domain.bean.repository.ImgListRepository;
import com.linkdemo.domain.bean.repository.LinkidLicenseManagerRepository;
import com.linkdemo.domain.bean.repository.PopupItemRepository;
import com.linkdemo.domain.bean.repository.StickyItemRepository;
import com.linkdemo.domain.bean.repository.TooltipItemRepository;
import com.linkdemo.domain.bean.table.DropZone;
import com.linkdemo.domain.bean.table.ImgList;
import com.linkdemo.domain.bean.table.LinkidLicenseManager;
import com.linkdemo.domain.bean.table.PopupItem;
import com.linkdemo.domain.bean.table.StickyItem;
import com.linkdemo.domain.bean.table.TooltipItem;
import com.linkdemo.service.AddImgToDropZoneList;

@Controller
public class LinkDemoController {
	@Autowired
	HttpSession session;

	@Autowired
	PopupItemRepository popupRepository;
	@Autowired
	StickyItemRepository stickyRepository;
	@Autowired
	DropZoneRepository dropzoneRepository;
	@Autowired
	TooltipItemRepository tooltipRepository;
	@Autowired
	ImgListRepository ImgListRepository;
	@Autowired
	LinkidLicenseManagerRepository linkidLicenseManagerRepository;


	@RequestMapping(value="/show",method=RequestMethod.GET)
		public ModelAndView showLinkdemo(@ModelAttribute("isRegisterd") String isRegisterd,ModelAndView mav, @RequestParam("id") String linkid) throws Exception{



		List<PopupItem> popupList = new ArrayList<PopupItem>();
		List<StickyItem> stickyList = new ArrayList<StickyItem>();
		List<TooltipItem> tooltipList = new ArrayList<TooltipItem>();
		List<DropZone> dropzoneList = new ArrayList<DropZone>();
		List<ImgList> imgList = new ArrayList<ImgList>();

		popupList = popupRepository.findBylinkidIs(linkid);
		stickyList = stickyRepository.findBylinkidIs(linkid);
		dropzoneList = dropzoneRepository.findBylinkidIs(linkid);
		tooltipList = tooltipRepository.findBylinkidIs(linkid);
		imgList = ImgListRepository.findBylinkidIs(linkid);


		LinkidLicenseManager llm = linkidLicenseManagerRepository.findByLinkidIs(linkid);



		AddImgToDropZoneList addImgToDropZoneList = new AddImgToDropZoneList();
		List<DropZoneWithImgBean> dropzoneWithImglist = addImgToDropZoneList.dropzoneWithImglist(dropzoneList,imgList,llm);


		String demoname =  llm.getName();
		//System.out.println(demoname);


				mav.setViewName("linkdemo");
				mav.addObject("demoname",demoname);
				mav.addObject("popupList",popupList);
				mav.addObject("stickyList",stickyList);
				mav.addObject("tooltipList",tooltipList);
				mav.addObject("dropzoneList",dropzoneWithImglist);
				mav.addObject("imgList",imgList);
			return mav;
		}


}
