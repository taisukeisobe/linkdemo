package com.linkdemo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests()
				.antMatchers("/css/**","/assets/**", "/img/**", "/js/**", "/edit/begin", "/validate", "/edit/uploaditemstat",
						"/edit/uploadimg", "/register","/show","/")
				.permitAll().anyRequest().authenticated().and().csrf()
				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

		httpSecurity.formLogin().loginProcessingUrl("/login").loginPage("/edit/begin").usernameParameter("user")
				.passwordParameter("pass").permitAll().defaultSuccessUrl("/edit/begin").failureUrl("/edit/begin?error");


		httpSecurity.rememberMe().tokenValiditySeconds(60 * 60 * 24 * 30);

	httpSecurity.logout().logoutUrl("/logout").logoutSuccessUrl("/edit/begin")
		.deleteCookies("JSESSIONID", "remember-me").permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(this.userDetailsService)
				.passwordEncoder(new BCryptPasswordEncoder());
		;
	}

	@Autowired
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
}