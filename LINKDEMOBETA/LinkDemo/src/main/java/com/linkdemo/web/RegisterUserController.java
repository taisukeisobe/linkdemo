package com.linkdemo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkdemo.domain.bean.repository.MemberRepository;
import com.linkdemo.domain.bean.repository.TmpMemberRepository;
import com.linkdemo.domain.bean.table.TmpMember;
import com.linkdemo.service.util.UuidUtil;
import com.linkdemo.service.util.myIP;

@RestController
public class RegisterUserController {

	@Autowired
	TmpMemberRepository tmpMemberRepository;
	@Autowired
	MemberRepository memberRepository;
	@Autowired
	JavaMailSender mailSender;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@RequestParam("user") String user, @RequestParam("pass") String pass,
			@RequestParam("displyname") String displyname) throws Exception {

		String status = "ng";


		if ((!user.isEmpty() & user != null) & (!pass.isEmpty() & pass != null)
				& (!displyname.isEmpty() & displyname != null)) {



			boolean isMember = memberRepository.existsByUsername(user);


			if(!isMember){
			String vali = UuidUtil.generateUUID();
			BCryptPasswordEncoder passEncoder = new BCryptPasswordEncoder();




			try {
				TmpMember tmpMember = new TmpMember(user, passEncoder.encode(pass), displyname, vali);
				tmpMemberRepository.saveAndFlush(tmpMember);
			} catch (Exception e) {
				e.printStackTrace();
				//status = "エラー：DB保存失敗";
				return status;
			}

			String IPadnPort = myIP.getYourIP();
			String from = "info@tobidemo.com";
			String title = "Tobidemo アカウント確認のお願い";
			String content = displyname + "さん" + "\n" + "\n" + "以下のリンクにアクセスしてアカウントを認証してください" + "\n"
					+"http://" + IPadnPort
					+ "/validate"+ "?id=" + vali ;

					//+ "<a href =" + "'http:" + IPadnPort
					//+ "/validate"+ "?id=" + vali + "'>" + "クリックして認証" + "</a>" +
					//"\n\n" + "Tobidemo";

			try {
				SimpleMailMessage msg = new SimpleMailMessage();

				msg.setFrom(from);
				msg.setTo(user);
				msg.setSubject(title);// タイトルの設定
				msg.setText(content); // 本文の設定
				mailSender.send(msg);
			} catch (Exception e) {
				e.printStackTrace();
				//status = "エラー：メール送付失敗";
				return status;
			}

			status = "ok";
			}
			return status; //ng

		}
		;
		return status; //ng

	}


}
