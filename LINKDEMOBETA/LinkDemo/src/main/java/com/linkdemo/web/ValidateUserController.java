package com.linkdemo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.linkdemo.domain.bean.repository.MemberRepository;
import com.linkdemo.domain.bean.repository.TmpMemberRepository;
import com.linkdemo.domain.bean.table.Member;
import com.linkdemo.domain.bean.table.TmpMember;

@Controller
public class ValidateUserController {

	@Autowired
	TmpMemberRepository tmpMemberRepository;

	@Autowired
	MemberRepository memberRepository;

	@CrossOrigin
	@RequestMapping(value = "/validate", method = RequestMethod.GET)
	public String validate(RedirectAttributes redirectAttributes,ModelAndView mav, @RequestParam("id") String id) throws Exception {

		String isRegisterd = "false";
		boolean isExist = tmpMemberRepository.existsByValidation(id);


		//System.out.println(isExist);

		if (isExist) {
			try {
				TmpMember tmp = tmpMemberRepository.findByValidation(id);
				String username = tmp.getUsername();
				String displyname = tmp.getDisplyname();
				String password = tmp.getPassword();


				Member member = new Member();
				member.setDisplyname(displyname);
				member.setPassword(password);
				member.setUsername(username);

				memberRepository.saveAndFlush(member);

				isRegisterd = "true";

			} catch (Exception e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				 isRegisterd = "false";
			}


		}
		redirectAttributes.addFlashAttribute("isRegisterd", isRegisterd);
		 return "redirect:/edit/begin";
	}

}
