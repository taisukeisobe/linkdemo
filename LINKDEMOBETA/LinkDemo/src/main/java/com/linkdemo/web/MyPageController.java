package com.linkdemo.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.linkdemo.domain.bean.repository.LinkidLicenseManagerRepository;
import com.linkdemo.domain.bean.table.LinkidLicenseManager;
import com.linkdemo.domain.bean.table.Member;

@Controller

public class MyPageController {
	@Autowired
	LinkidLicenseManagerRepository linkidLicenseManagerRepository;

	@RequestMapping(value = "/mypage", method = RequestMethod.GET)
	public ModelAndView mypage(ModelAndView mav, @AuthenticationPrincipal Member member) {

		int userid = member.getUserid();
		List<LinkidLicenseManager> linklist = new ArrayList<LinkidLicenseManager>();

		linklist = linkidLicenseManagerRepository.findByUserid(userid);

		mav.addObject("Linklist",linklist);
		mav.setViewName("mypage");

		return mav;
	}
}
