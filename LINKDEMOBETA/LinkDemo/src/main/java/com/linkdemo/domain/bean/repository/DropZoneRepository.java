package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.DropZone;


@Repository
public interface DropZoneRepository extends JpaRepository<DropZone,String>{

	 List<DropZone> findBylinkidIs(String id);



}


