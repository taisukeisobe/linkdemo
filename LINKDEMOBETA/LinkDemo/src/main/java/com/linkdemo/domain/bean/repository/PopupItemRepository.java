package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.PopupItem;


	@Repository
	public interface PopupItemRepository extends JpaRepository<PopupItem,String>{

		 List<PopupItem> findBylinkidIs(String id);


	}

