package com.linkdemo.domain.bean;

public class DropZoneWithImgBean {

	private String id;

	private String linkid;

	private String dropzone;

	private String img;

	private String Base64Img;

	private int width;

	private int height;

	public DropZoneWithImgBean(String id, String linkid, String dropzone, String img, String base64Img,int width, int height) {
		super();
		this.id = id;
		this.linkid = linkid;
		this.dropzone = dropzone;
		this.img = img;
		this.Base64Img = base64Img;
		this.width =width;
		this.height =height;


	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLinkid() {
		return linkid;
	}

	public void setLinkid(String linkid) {
		this.linkid = linkid;
	}

	public String getDropzone() {
		return dropzone;
	}

	public void setDropzone(String dropzone) {
		this.dropzone = dropzone;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getBase64Img() {
		return Base64Img;
	}

	public void setBase64Img(String base64Img) {
		Base64Img = base64Img;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}




}
