package com.linkdemo.domain.bean.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.linkdemo.domain.bean.table.TmpMember;

public interface TmpMemberRepository extends JpaRepository<TmpMember, Integer> {
	public boolean existsByValidation(String validation);
	public boolean existsByUsername(String username);
    public TmpMember findByValidation(String validation);


}
