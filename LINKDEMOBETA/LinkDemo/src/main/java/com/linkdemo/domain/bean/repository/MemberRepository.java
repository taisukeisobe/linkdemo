package com.linkdemo.domain.bean.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.linkdemo.domain.bean.table.Member;




	public interface MemberRepository extends JpaRepository<Member, Integer>
	{
	    public Member findByUsername(String username);
	    public boolean existsByUsername(String username);

	}
