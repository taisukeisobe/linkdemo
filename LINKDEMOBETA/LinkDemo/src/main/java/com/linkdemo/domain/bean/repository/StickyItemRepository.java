package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.StickyItem;

@Repository
public interface StickyItemRepository extends JpaRepository<StickyItem,String>{

	 List<StickyItem> findBylinkidIs(String id);

}
