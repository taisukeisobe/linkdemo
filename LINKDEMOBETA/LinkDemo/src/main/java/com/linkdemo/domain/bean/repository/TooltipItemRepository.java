package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.TooltipItem;

@Repository
public interface TooltipItemRepository extends JpaRepository<TooltipItem,String>{

	 List<TooltipItem> findBylinkidIs(String id);


}
