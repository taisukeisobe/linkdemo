package com.linkdemo.domain.bean.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dropzone")
public class DropZone implements Serializable {

	private static final long serialVersionUID = 1L;


	public DropZone(){}

	public DropZone(String id, String linkid, String dropzone, String img) {
		super();
		this.id = id;
		this.linkid = linkid;
		this.dropzone = dropzone;
		this.img = img;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLinkid() {
		return linkid;
	}

	public void setLinkid(String linkid) {
		this.linkid = linkid;
	}

	public String getDropzone() {
		return dropzone;
	}

	public void setDropzone(String dropzone) {
		this.dropzone = dropzone;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Id
	private String id;

	@Column(name = "linkid")
	private String linkid;

	@Column(name = "dropzone")
	private String dropzone;

	@Column(name = "img")
	private String img;

}
