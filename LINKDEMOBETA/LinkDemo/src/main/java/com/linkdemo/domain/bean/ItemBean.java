package com.linkdemo.domain.bean;

public class ItemBean {

	/*
	type:type
	temid : itemid, //common
	top : top, //common
	left : left, //common
	color : color, //popup,tooltip
	size : size, //popup,tooltip
	title : title//tooltip
	target : target//popup
	width : width, //sticky
	height : height, //sticky
	text : text //sticky
*/
	    //private String linkid;
		private String id;
		private String type;
		private String itemid;
		private int top;
		private int left;
		private String color;
		private String size;
		private String title;
		private String target;
		private int width;
		private int height;
		private String text;
		private String  dropzone;
		private String  img;

		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}

		public String getDropzone() {
			return dropzone;
		}
		public void setDropzone(String dropzone) {
			this.dropzone = dropzone;
		}
		public String getImg() {
			return img;
		}
		public void setImg(String img) {
			this.img = img;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getItemid() {
			return itemid;
		}
		public void setItemid(String itemid) {
			this.itemid = itemid;
		}
		public int getTop() {
			return top;
		}
		public void setTop(int top) {
			this.top = top;
		}
		public int getLeft() {
			return left;
		}
		public void setLeft(int left) {
			this.left = left;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public String getSize() {
			return size;
		}
		public void setSize(String size) {
			this.size = size;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getTarget() {
			return target;
		}
		public void setTarget(String target) {
			this.target = target;
		}
		public int getWidth() {
			return width;
		}
		public void setWidth(int width) {
			this.width = width;
		}
		public int getHeight() {
			return height;
		}
		public void setHeight(int height) {
			this.height = height;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}




	}


