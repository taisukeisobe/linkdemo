package com.linkdemo.domain.bean.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stickyitem")
public class StickyItem implements Serializable {

	private static final long serialVersionUID = 1L;

	public StickyItem(){}


	public StickyItem(String id, String linkid, String itemid, String color, int height, int width, int top,
			int leftposition, String text) {
		super();
		this.id = id;
		this.linkid = linkid;
		this.itemid = itemid;
		this.color = color;
		this.height = height;
		this.width = width;
		this.top = top;
		this.leftposition = leftposition;
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLinkid() {
		return linkid;
	}

	public void setLinkid(String linkid) {
		this.linkid = linkid;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

	public int getLeftposition() {
		return leftposition;
	}

	public void setLeftposition(int leftposition) {
		this.leftposition = leftposition;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Id
	private String id;

	@Column(name = "linkid")
	private String linkid;

	@Column(name = "itemid")
	private String itemid;

	@Column(name = "color")
	private String color;

	@Column(name = "height")
	private int height;

	@Column(name = "width")
	private int width;

	@Column(name = "top")
	private int top;

	@Column(name = "leftposition")
	private int leftposition;

	@Column(name = "text")
	private String text;

}
