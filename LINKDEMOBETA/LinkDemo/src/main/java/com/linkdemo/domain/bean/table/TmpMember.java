package com.linkdemo.domain.bean.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tmpmember")
public class TmpMember {


		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    private Long userid;

	  //  @Column(nullable = false, unique = true)
		@Column(nullable = false)
	    private String username;

	    @Column(nullable = false)
	    private String password;

	    @Column(nullable = false)
	    private String displyname;


	    @Column(nullable = false)
	    private String validation;

	    public TmpMember() {
			super();

		}
		public TmpMember(String username, String password, String displyname, String validation) {
			super();
			this.username = username;
			this.password = password;
			this.displyname = displyname;
			this.validation = validation;
		}





		public String getUsername() {
			return username;
		}


		public void setUsername(String username) {
			this.username = username;
		}


		public String getPassword() {
			return password;
		}


		public void setPassword(String password) {
			this.password = password;
		}


		public String getDisplyname() {
			return displyname;
		}


		public void setDisplyname(String displyname) {
			this.displyname = displyname;
		}


		public String getValidation() {
			return validation;
		}


		public void setValidation(String validation) {
			this.validation = validation;
		}

}
