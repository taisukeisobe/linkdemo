package com.linkdemo.domain.bean.table;



	import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

	@Entity
	@Table(name = "linkidlicensemanager")
	public class LinkidLicenseManager implements Serializable {

		private static final long serialVersionUID = 1L;

		@Id
		private String linkid;

		@Column(name = "userid")
		private int userid;

		@Column(name = "status")
		private boolean status;

		@Column(name = "name")
		private String name;

		@Column(name = "time")
		private Timestamp time;


		public LinkidLicenseManager() {
			super();

		}

		public LinkidLicenseManager(String linkid, int userid, boolean status,String name,Timestamp time) {
			super();
			this.linkid = linkid;
			this.userid = userid;
			this.status = status;
			this.name = name;
			this.time = time;

		}

		public String getLinkid() {
			return linkid;
		}

		public void setLinkid(String linkid) {
			this.linkid = linkid;
		}

		public int getUserid() {
			return userid;
		}

		public void setUserid(int userid) {
			this.userid = userid;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}


		public Timestamp getTime() {
			return time;
		}


		public void setTime(Timestamp time) {
			this.time = time;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}






}
