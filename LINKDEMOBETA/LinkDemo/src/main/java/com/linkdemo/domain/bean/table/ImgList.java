package com.linkdemo.domain.bean.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "imglist")
public class ImgList implements Serializable {


	private static final long serialVersionUID = 1L;

	public ImgList(){}

	@Id
	private String imgid;

	@Column(name = "linkid")
	private String 	linkid;

	@Column(name = "width")
	private int width;

	@Column(name = "height")
	private int height;




	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getImgid() {
		return imgid;
	}

	public void setImgid(String imgid) {
		this.imgid = imgid;
	}

	public String getlinkid() {
		return linkid;
	}

	public void setlinkid(String linkid) {
		this.linkid = linkid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public ImgList(String imgid, String linkid,int width,int height) {
		super();
		this.imgid = imgid;
		this.linkid = linkid;
		this.width = width;
		this.height = height;
	}




}
