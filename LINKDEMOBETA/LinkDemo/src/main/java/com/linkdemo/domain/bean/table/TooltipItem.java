package com.linkdemo.domain.bean.table;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tooltipitem")
public class TooltipItem implements Serializable{

	private static final long serialVersionUID = 1L;

	public TooltipItem(){}

	public TooltipItem(String id, String linkid, String itemid, String color, String size, int top, int leftposition,
			String title) {
		super();
		this.id = id;
		this.linkid = linkid;
		this.itemid = itemid;
		this.color = color;
		this.size = size;
		this.top = top;
		this.leftposition = leftposition;
		this.title = title;
	}


	@Id
	private String id;

	@Column(name = "linkid")
	private String linkid;

	@Column(name = "itemid")
	private String itemid;

	@Column(name = "color")
	private String color;

	@Column(name = "size")
	private String size;

	@Column(name = "top")
	private int top;

	@Column(name = "leftposition")
	private int leftposition;

	@Column(name = "title")
	private String title;




	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLinkid() {
		return linkid;
	}

	public void setLinkid(String linkid) {
		this.linkid = linkid;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

	public int getLeftposition() {
		return leftposition;
	}

	public void setLeftposition(int leftposition) {
		this.leftposition = leftposition;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
