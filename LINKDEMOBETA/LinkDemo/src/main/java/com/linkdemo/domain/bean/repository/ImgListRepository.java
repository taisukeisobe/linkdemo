package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.ImgList;

@Repository
public interface ImgListRepository extends JpaRepository<ImgList,String>{

	 List<ImgList> findBylinkidIs(String linkid);
}

