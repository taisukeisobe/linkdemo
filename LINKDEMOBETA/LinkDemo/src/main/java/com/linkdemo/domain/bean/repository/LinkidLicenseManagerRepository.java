package com.linkdemo.domain.bean.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkdemo.domain.bean.table.LinkidLicenseManager;


	@Repository
	public interface LinkidLicenseManagerRepository extends JpaRepository<LinkidLicenseManager,String>{

		 public LinkidLicenseManager findByLinkidIs(String linkid);

		 public List<LinkidLicenseManager> findByUserid(int userid);



}
