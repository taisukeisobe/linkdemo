
$(function() {

	$(document).ready(function() {
		$('._tooltip').tooltipster({ theme: 'tooltipster-Light',
			 delay: 50});
	});

	// 「.modal-open」をクリック
	$(document).on(
			'click',
			"._popup",
			function() {
				// オーバーレイ用の要素を追加
				var popuptarget = $(this).attr('data-popuptarget');
				var overlay = '<div id="' + 'modal-overlay' + popuptarget
						+ '"></div>'
				var overlayid = '#' + 'modal-overlay' + popuptarget;
				$('body').append(overlay);
				// オーバーレイをフェードイン
				overlayobj = document.getElementById('modal-overlay' + popuptarget);
				$(overlayobj).fadeIn('fast');

				var popuptarget_id = '.' + popuptarget;
				var targetobj = document.getElementById(popuptarget);
				$(targetobj).fadeIn(1);

				$(overlayobj).off().click(function() {
					// モーダルコンテンツとオーバーレイをフェードアウト
					var targetobjout = document.getElementById(popuptarget);
					$(targetobjout).fadeOut(1);
					overlayobj = document.getElementById('modal-overlay' + popuptarget);
					$(overlayobj).fadeOut(1, function() {
						$(overlayobj).remove();

					});
				});

			});
});