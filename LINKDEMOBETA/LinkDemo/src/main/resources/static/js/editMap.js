$(function() {

	// ------------グローバル変数--------------
	TRANSITION_MANAGER = [ '1.1' ]; // 現在地TRANSITION_MANAGER[TRANSITION_MANAGER.length
	// - 1]
	UPLOADED_IMG_PASS = null;
	// ---------------------------------------

	// for sticky edting
	$(document).on('click', ".editsticky", function() {
		$(this).hide();
		$('.boxsticky').addClass('editable');
		$(".overlaysticky").hide();
		// $('.text').attr('contenteditable', 'true');
		$('.savesticky').show();
	});

	$(document).on('click', ".savesticky", function() {
		$(this).hide();
		$('.boxsticky').removeClass('editable');
		$(".overlaysticky").show();

		// $('.text').removeAttr('contenteditable');
		$('.editsticky').show();
	});

	/**
	 * $(document).on("keydown", 'div[contenteditable]', function(e) { // trap
	 * the return key being pressed if (e.keyCode === 13) { // insert 2 br tags
	 * (if only one br tag is inserted the cursor won't // go to the next line)
	 * document.execCommand('insertHTML', false, '</br>'); // prevent the
	 * default behaviour of return key pressed return false; } });
	 */

	// ユーザ登録
	$(".regiuser-ajax-btn").on("submit", function(e) {

		e.preventDefault();

		var displyname = $(".displyname-register").val();
		var user = $(".user-register").val();
		var pass = $(".pass-register").val();
		var xsrf = $.cookie('XSRF-TOKEN');

		if(!user |!displyname | !pass){
			return false;
		}

		if(!isEmail(user) | displyname.replace(/[\t\s ]/g, '').length < 1 | pass.replace(/[\t\s ]/g, '').length < 4){
			return false;
		}

		$.ajax({
			url : "/register",
			type : "POST",
			dataType : "text",
			//contentType : 'application/json; charset=utf-8',
			data : {
				"user" : user,
				"pass" : pass,
				"displyname" : displyname,
				"beforeSend"	: function(xhr, setting) {
					$(".btn").attr('disabled', true);
					$(".login-loading-animation").removeClass("nodispy");

				}
			},
			timeout : 10000,
			headers : {
				"X-XSRF-TOKEN" : xsrf
			}



		}).done(function(data) { // Ajax通信が成功した時の処理
			if(data === ("ng")){
				alert("登録に失敗しました。メールアドレスは正しいかご確認下さい。既にユーザー作成済の場合は、ログインして下さい");


			}else{
			$.magnificPopup.close();
			$('.prompt-varificcation').slideDown(1500);
			};
			$(".login-loading-animation").addClass("nodispy");
			$(".btn").attr('disabled', false);
		}).fail(function(XMLHttpRequest, textStatus, errorThrown) { // Ajax通信が失敗した時の処理
			alert(textStatus);
			$(".login-loading-animation").addClass("nodispy");
			$(".btn").attr('disabled', false);
		});

	});

	// ファイルアップロード

	$('#upload_file')
			.on(
					'change',
					function(event) {
						event.preventDefault();

						if (window.FormData) {
							var ajaxUrl = "/edit/uploadimg";
							var uploadFile = $('#upload_file')[0].files[0];
							var formData = new FormData();
							formData.append("file", uploadFile);

							var xsrf = $.cookie('XSRF-TOKEN');

							$.ajax({
										type : "POST", // HTTP通信の種類
										url : ajaxUrl, // リクエストを送信する先のURL
										dataType : "text", // サーバーから返されるデータの型
										data : formData,// サーバーに送信するデータ
										cache : false,
										async : false,
										processData : false,
										"beforeSend"	: function(xhr, setting) {
											$(".btn").attr('disabled', true);
											$(".img-uploading-animation").removeClass("nodispy");
										},
										contentType : false,
										headers : {
											'X-XSRF-TOKEN' : xsrf
										}
									})
									.done(
											function(data) {
												$(".btn").attr('disabled', false);
												$(".img-uploading-animation").addClass("nodispy");

												if (data != "false") {
													var targetpageimg = $('[data-dropzone="'
															+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1]
															+ '"] img');

													var targetpage = $('[data-dropzone="'
															+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1]
															+ '"]');

													targetpage.attr("data-img",
															data);

													var reader = new FileReader();
													reader
															.readAsDataURL(event.target.files[0]);
													reader.onload = function(e) {
														targetpageimg
																.attr(
																		'src',
																		e.target.result);
													}

												} else {
													$('.upload-img-failed').slideDown(1500);
												}

											}).fail(
											function(XMLHttpRequest,
													textStatus, errorThrown) { // Ajax通信が失敗した時の処理
												$('.upload-img-failed').slideDown(1500);
												$(".btn").attr('disabled', false);
												$(".img-uploading-animation").addClass("nodispy");
											});
						} else {
							alert("アップロードに対応できていないブラウザです。");
						}

					});

	// ドラッグ
	counts = [ 0 ]
	$(".dragtgt").draggable({
		helper : 'clone'
	});

	// ドロップ後のイベント
	$('.maperea')
			.droppable(
					{
						drop : function(event, ui) {
							if (ui.draggable.hasClass("dropped") == false) {
								var clone = ui.draggable.clone().draggable();
								if (ui.draggable.hasClass("boxsticky") == true) {
									clone
											.append("<textarea  class='textsticky textsticky_color_white'></textarea>");
									clone
											.append("<div  class='editsticky'>edit</div>");
									clone
											.append("<div  class='savesticky'>save</div>");
									clone.resizable();

								}
								var levelandpage = $(this)
										.attr('data-dropzone'); // 1.1
								var itemleng = ($(this).children().length);
								var itemid = levelandpage + "." + itemleng;
								var leftPosition = (ui.offset.left - 110);
								var topPosition = (ui.offset.top - 60);

								clone.css('left', leftPosition);
								clone.css('top', topPosition);
								clone.attr("data-item", itemid);

								// clone.addClass("item-" + itemid);
								clone.addClass("dropped");
								// clone.addClass("");
								clone.appendTo(this);// ドロップされたオブジェクトを複製して自分に追加
								$('.dropped').css('position', "absolute");// relativeがついてしまうので、変換
							}
						},
					});

	// 全てのオブジェクトの情報を収集

	/*
	 * top : top, //common left : left, //common itemid : itemid, //common color :
	 * color, //popup,tooltip size : size, //popup,tooltip title :
	 * title//tooltip target : target//popup width : width, //sticky height :
	 * height, //sticky text : text //sticky
	 *
	 *
	 * type:type itemid : itemid, //common top : top, //common left : left,
	 * //common color : color, //common size : size, //popup,tooltip title :
	 * title//tooltip target : target//popup width : width, //sticky height :
	 * height, //sticky text : text //sticky
	 *
	 */

	$(document)
			.on(
					'submit',
					".save_to_server",
					function(e) {
						e.preventDefault();

						$(".maperea").show(); // disply:noneだとtop,left が取れない

						var demoname = $('.demo-name-val').val();
						if (demoname === undefined || demoname === null | demoname.replace(/[\t\s ]/g, '').length < 1){
							demoname = "-";
						}
						var item_stat_array = [];

						var maperea_array = $('.maperea');
						var maperea_leng = $('.maperea').length;
						$(maperea_array[i]).attr('data-dropzone');
						var linkid = $('#linkid').text();
						var xsrf = $.cookie('XSRF-TOKEN');

						for (var i = 0; i < maperea_leng; i++) {

							var dropzone = $(maperea_array[i]).attr(
									'data-dropzone');
							var img = $(maperea_array[i]).attr('data-img')
							if (img === undefined || img === null) {
								img = "0";
							}
							// var id = linkid + "_" +dropzone; //データベース保管時の主キー

							item_stat_array.push({
								id : linkid + "_" + dropzone,
								type : "drop-zone", // common
								itemid : "0", // common
								top : "0", // common
								left : "0", // common
								color : "0", // common
								size : "0", // popup,tooltip
								title : "0", // tooltip
								target : "0", // popup
								width : "0", // sticky
								height : "0", // sticky
								text : "0", // sticky
								dropzone : dropzone, // dropzpne
								img : img
							// dropzone

							});
						}

						var targetArray = $('.dropped');
						var item_num = targetArray.length;
						item_num - 1;

						for (var i = 0; i < item_num; i++) {
							var target = targetArray[i];

							if (target.className.indexOf('_tooltip') != -1) {

								var itemid = $(target).attr("data-item");
								var top = $(target).position().top;
								var left = $(target).position().left;
								var color = $(target).attr(
										"data-tooltipIconColor");
								var size = $(target).attr(
										"data-tooltipIconSize");
								var title = $(target).attr("title");
								// var id = linkid + "_" + itemid;
								// //データベース保管時の主キー

								item_stat_array.push({
									id : linkid + "_" + itemid,
									type : "_tooltip", // common
									itemid : itemid, // common
									top : top, // common
									left : left, // common
									color : color, // common
									size : size, // popup,tooltip
									title : title, // tooltip
									target : "0", // popup
									width : "0", // sticky
									height : "0", // sticky
									text : "0", // sticky
									dropzone : "0", // dropzpne
									img : img
								// dropzone
								});

							} else if (target.className.indexOf('_popup') != -1) {

								var top = $(target).position().top;
								var left = $(target).position().left;
								var itemid = $(target).attr("data-item");
								var color = $(target).attr("data-popupColor");
								var size = $(target).attr("data-popupSize");
								var target = $(target).attr("data-popuptarget");

								item_stat_array.push({
									id : linkid + "_" + itemid,
									type : "_popup", // common
									itemid : itemid, // common
									top : top, // common
									left : left, // common
									color : color, // common
									size : size, // popup,tooltip
									title : "0", // tooltip
									target : target, // popup
									width : "0", // sticky
									height : "0", // sticky
									text : "0", // sticky
									dropzone : "0", // dropzpne
									img : img
								// dropzone
								});

							} else if (target.className.indexOf('_sticky') != -1) {

								var top = $(target).position().top;
								var left = $(target).position().left;
								var width = $(target).width();
								var height = $(target).height();
								var itemid = $(target).attr("data-item");
								var color = $(target).attr("data-stickycolor");
								var text = $(target).children(".textsticky")
										.val();

								item_stat_array.push({
									id : linkid + "_" + itemid,
									type : "_sticky", // common
									itemid : itemid, // common
									top : top, // common
									left : left, // common
									color : color, // common
									size : "0", // popup,tooltip
									title : "0", // tooltip
									target : "0", // popup
									width : width, // sticky
									height : height, // sticky
									text : text, // sticky
									dropzone : "0", // dropzpne
									img : img
								// dropzone
								});

							}

						}

						$(".maperea").hide();
						$(
								'[data-dropzone="'
										+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1]
										+ '"]').show();

						if (window.FormData) {

							var ajaxUrl = "/edit/uploaditemstat" + "?name=" + demoname ;

							$
									.ajax(
											{
												type : "POST", // HTTP通信の種類
												url : ajaxUrl, // リクエストを送信する先のURL
												dataType : "json", // サーバーから返されるデータの型
												data : JSON
														.stringify(item_stat_array),
												contentType : 'application/json; charset=utf-8',
												"beforeSend": function(xhr, setting) {
													$(".btn").attr('disabled', true);
													$(".item-uploading-animation").removeClass("nodispy");
												},
												headers : {
													'X-XSRF-TOKEN' : xsrf
												},
											})

									.done(
											function(data) { // Ajax通信が成功した時の処理

												var yourLink = "/show?id=";
												var yourLinkID = $("#linkid")
														.text();

												var demoname = $(".demo-name-val").val();
												if(demoname==null ||demoname== "") {demoname = "無題のトビデモ"}

												$(".yourLink").attr("href",
														yourLink + yourLinkID);
												$(".yourLink").text(
														demoname);
												$(".yourLink").attr('target',
														'_blank');
												$('.upload-item-done').slideDown(1500);

												$(".btn").attr('disabled', false);
												$(".item-uploading-animation").addClass("nodispy");


											}).fail(
											function(XMLHttpRequest,
													textStatus, errorThrown) { // Ajax通信が失敗した時の処理
												$(".btn").attr('disabled', false);
												$(".item-uploading-animation").addClass("nodispy");
												$('.upload-item-failed').slideDown(1500);
												return false;

											});
						} else {
							alert("アップロードに対応できていないブラウザです。");
						}

					});

	/**
	 * //バックグラウンド画像の縦横pxを取得 画像そのままだすことにしたからつかわないかも $(function getBKheight(){
	 * //画像の縦の長さを取得 var img = new Image(); var bkwidth = 1200;
	 * //$("#wrmaperea").css("width"); // img.src =
	 * $('#maperea').css('background-image').replace(/url\(|\)$/ig, ""); img.src =
	 * "./../PHOTO/dashbord.png" var bkheight = 1200 * img.height / img.width;
	 * });
	 */

	/**
	 * $('.save').on('click', function() { var iconstats = {}; for (var i = 0; i <
	 * counts[0]; i++) { var target = $(".item-" + i).position(); var targettop =
	 * target.top; var targetleft = target.left iconstats[i] = { top :
	 * targettop, left : targetleft }; } });
	 */

	$(document).on('dblclick', ".dropped", function() {
		$(".modal-overlay").show();
		$(".slideContentContainer").show();
		// 編集対象によって表示する項目と値を更新

		var _this = this;

		var icontype = iconTypeIs(_this);
		$(this).addClass("flag_now_edit")

		$(".forEditItem").hide();

		if (icontype === "_popup") {

			$(".forPopup").show();

			var nowSize = $(this).attr("data-popupsize");
			var nowColor = $(this).attr("data-popupcolor");

			if (nowSize == "small") {

				$('input[name=iconSize]:eq(1)').prop('checked', false);
				$('input[name=iconSize]:eq(2)').prop('checked', false);
				$('input[name=iconSize]:eq(0)').prop('checked', true);

			} else if (nowSize == "normal") {
				$('input[name=iconSize]:eq(0)').prop('checked', false);

				$('input[name=iconSize]:eq(2)').prop('checked', false);
				$('input[name=iconSize]:eq(1)').prop('checked', true);

			} else if (nowSize == "big") {
				$('input[name=iconSize]:eq(0)').prop('checked', false);
				$('input[name=iconSize]:eq(1)').prop('checked', false);
				$('input[name=iconSize]:eq(2)').prop('checked', true);
			}

			$(".iconColor").val(nowColor);

		} else if (icontype === "_tooltip") {

			$(".forTooltop").show();

			var nowSize = $(this).attr("data-tooltipiconsize");
			var nowColor = $(this).attr("data-tooltipiconcolor");
			var nowMemo = $(this).attr("title");

			$(".iconColor").val()

			if (nowSize == "small") {

				$('input[name=iconSize]:eq(1)').prop('checked', false);
				$('input[name=iconSize]:eq(2)').prop('checked', false);
				$('input[name=iconSize]:eq(0)').prop('checked', true);

			} else if (nowSize == "normal") {
				$('input[name=iconSize]:eq(0)').prop('checked', false);

				$('input[name=iconSize]:eq(2)').prop('checked', false);
				$('input[name=iconSize]:eq(1)').prop('checked', true);

			} else if (nowSize == "big") {
				$('input[name=iconSize]:eq(0)').prop('checked', false);
				$('input[name=iconSize]:eq(1)').prop('checked', false);
				$('input[name=iconSize]:eq(2)').prop('checked', true);
			}

			$(".iconColor").val(nowColor);

			$(".tooltipmemo").val(nowMemo);

		} else if (icontype === "_sticky") {

			$(".forSticky").show();

			var nowColor = $(this).attr("data-stickycolor");
			$(".iconColor").val(nowColor);
		}

		// オブジェクトのステータス更新
		$('.iconStatSave').on('click', function() {
			var iconsize = $("[name=iconSize]:checked").val();
			var iconcolor = $(".iconColor").val();
			var tooltipMemo = $(".tooltipmemo").val();
			var iconobj = {
				size : iconsize,
				color : iconcolor,
				tooltipMemo : tooltipMemo,
				type : icontype,
				_this : _this
			};
			changeIconStat(iconobj, _this);
			hideModal();

			// イベントの中にイベントを書くと、内側のイベントが実行回数だけ重複して登録される問題への対策。
			$('.document').off('dblclick', ".dropped");
			$('.iconStatSave').off('click');
		});
	});

	// ポップアップ先の要素を作成
	$('.edit_popup_target')
			.on(
					'click',
					function() {

						var _this = $('.flag_now_edit');

						var page_level_num = TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1];
						var page_level = page_level_num.slice(0, 1);
						var next_page_level = Number(page_level) + 1;
						var page_num = page_level_num.slice(2, 3);
						var next_page_num = $('[data-dropzone^="'
								+ next_page_level + '"]').length;

						if ($(_this).attr("data-popuptarget") == undefined) {

							if (next_page_num != 0) {
								next_page_num += 1;
							} else {
								next_page_num = 1;

							}
							var this_dropzone_id = page_level + "." + page_num;
							var next_dropzone_id = next_page_level + "."
									+ next_page_num;

							$('.mapEreaWrapper')
									.append(
											'<div class="maperea" data-dropzone="'
													+ next_dropzone_id
													+ '"><img src ="./../../img/default-none.png"></div>');
							$(_this).attr("data-popuptarget", next_dropzone_id);

							// ------------------------------------------
							$('.maperea')
									.droppable(
											{
												drop : function(event, ui) {
													if (ui.draggable
															.hasClass("dropped") == false) {
														var clone = ui.draggable
																.clone()
																.draggable();
														if (ui.draggable
																.hasClass("boxsticky") == true) {
															clone
																	.append("<textarea class='textsticky textsticky_color_white'>編集して</textarea>");
															clone
																	.append("<div  class='editsticky'>edit</div>");
															clone
																	.append("<div  class='savesticky'>save</div>");
															clone.resizable();

														}

														var levelandpage = $(
																this)
																.attr(
																		'data-dropzone'); // 1.1
														var itemleng = ($(this)
																.children().length);
														var itemid = levelandpage
																+ "."
																+ itemleng;
														var leftPosition = (ui.offset.left - 110);
														var topPosition = (ui.offset.top - 60);
														clone.css('left',
																leftPosition);
														clone.css('top',
																topPosition);
														// clone.addClass("item-"
														// + itemid);
														clone.attr("data-item",
																itemid);
														clone
																.addClass("dropped");
														// clone.addClass("");
														clone.appendTo(this);// ドロップされたオブジェクトを複製して自分に追加
														$('.dropped').css(
																'position',
																"absolute");// relativeがついてしまうので、変換
													}
												},
											});
							// ------------------------------------------

							// 現在のページを非表示にして次ページに移動する
							// $(_this).removeClass("flag_now_edit")

							TRANSITION_MANAGER.push(next_dropzone_id);
							changePageTransitionView();

							// $('[data-dropzone="'+this_dropzone_id +
							// '"]').hide();

						} else {

							// var this_dropzone_id = page_level + "." +
							// page_num;
							// var next_dropzone_id = next_page_level + "." +
							// next_page_num;

							TRANSITION_MANAGER.push($(_this).attr(
									"data-popuptarget"));
							changePageTransitionView();
						}
						$(_this).removeClass("flag_now_edit");
						// $('[data-dropzone="'+this_dropzone_id + '"]').hide();
						// $('[data-dropzone="'+next_dropzone_id + '"]').show();
						$(
								'[data-dropzone="'
										+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1]
										+ '"]').show();
						$(
								'[data-dropzone="'
										+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 2]
										+ '"]').hide();

						hideModal();
					});

	// ページを一つ戻る
	$('.backToBeforePage')
			.on(
					'click',
					function() {

						if (TRANSITION_MANAGER.length != 1) {
							$(
									'[data-dropzone="'
											+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 1]
											+ '"]').hide();
							$(
									'[data-dropzone="'
											+ TRANSITION_MANAGER[TRANSITION_MANAGER.length - 2]
											+ '"]').show();
							// ACTIVE_PAGE_NUMBER =
							// TRANSITION_MANAGER[TRANSITION_MANAGER.length-2];
							TRANSITION_MANAGER.pop();
							changePageTransitionView();
						}
						;
						return false;
					});

	// オブジェクト編集ウインドウ閉じる

	$('.move-to-register').on('click', function() {
		$('.ld-login').hide();
		$('.ld-register').show();

	});

	$('.closeSlide').on('click', function() {
		hideModal();
		// $(".modal-overlay").hide();
		// $(".slideContentContainer").hide();

	});
	// モーダル表示
	$('.modal-overlay').on('click', function() {
		hideModal();
		// $(".modal-overlay").hide();
		// $(".slideContentContainer").hide();

	});

	function changePageTransitionView() {
		var tr = null;
		for ( var i in TRANSITION_MANAGER) {
			if (TRANSITION_MANAGER.length - 1 != i) {
				if (tr == null) {
					tr = TRANSITION_MANAGER[i] + "→";
				} else {
					tr += TRANSITION_MANAGER[i] + "→";
				}
			} else {

				if(tr == null){
					tr = 1.1;
				}else{
				tr += TRANSITION_MANAGER[i];
				}
			}
		}

		$('.pageTransition').text(tr);
	}
	;

	function changeIconStat(iconobj, _this) {
		// var popupcolorlist = getPopupColorDetail(iconobj.color);

		if (iconobj.type === "_popup") {

			$(_this)
					.children(".circle")
					.removeClass(
							function(index, className) {
								return (className
										.match(/\b_popup_circle_color_\S+/g) || [])
										.join(' ');
							});
			$(_this).children(".circle").addClass(
					"_popup_circle_color_" + iconobj.color);

			$(_this)
					.children(".circleAnimation")
					.removeClass(
							function(index, className) {
								return (className
										.match(/\b_popup_circleAnimation_color_\S+/g) || [])
										.join(' ');
							});
			$(_this).children(".circleAnimation").addClass(
					"_popup_circleAnimation_color_" + iconobj.color);

			$(_this).attr('data-popupColor', iconobj.color);

			$(_this).removeClass(function(index, className) {
				return (className.match(/\b_popup_size_\S+/g) || []).join(' ');
			});
			$(_this).addClass("_popup_size_" + iconobj.size);

			$(_this)
					.children(".circle")
					.removeClass(
							function(index, className) {
								return (className
										.match(/\b_popup_circle_size_\S+/g) || [])
										.join(' ');
							});
			$(_this).children(".circle").addClass(
					"_popup_circle_size_" + iconobj.size);

			$(_this)
					.children(".circleAnimation")
					.removeClass(
							function(index, className) {
								return (className
										.match(/\b_popup_circleAnimation_size_\S+/g) || [])
										.join(' ');
							});
			$(_this).children(".circleAnimation").addClass(
					"_popup_circleAnimation_size_" + iconobj.size);

			$(_this).attr('data-popupSize', iconobj.size);

		} else if (iconobj.type === "_tooltip") {

			if ($(_this).attr("class").indexOf("tooltipstered") != -1) {
				$(_this).tooltipster('destroy');
			}

			$(_this).attr("title", iconobj.tooltipMemo);
			$(_this).tooltipster({
				theme : 'tooltipster-Light',
				delay : 50
			}); // ツールチップ有効化
			$(_this).attr("title", iconobj.tooltipMemo); // tooltipster({})にtitleが削除？されるからサーバー保存ように

			$(_this).children(".tooltipIcon").removeClass(
					function(index, className) {
						return (className.match(/\btooltipIcon_\S+/g) || [])
								.join(' ');
					});
			$(_this).children(".tooltipIcon").addClass(
					"tooltipIcon_" + iconobj.color);

			$(_this).attr('data-tooltipIconColor', iconobj.color);

			$(_this).children(".tooltipIcon").removeClass(
					function(index, className) {
						return (className.match(/\b_tooltip_size_\S+/g) || [])
								.join(' ');
					});
			$(_this).children(".tooltipIcon").addClass(
					"_tooltip_size_" + iconobj.size);

			$(_this).addClass("_tooltip_size_" + iconobj.size);

			$(_this).attr('data-tooltipIconSize', iconobj.size);

		} else if (iconobj.type === "_sticky") {
			$(_this)
					.removeClass(
							function(index, className) {
								return (className
										.match(/\bboxsticky_color_\S+/g) || [])
										.join(' ');
							});
			$(_this).addClass("boxsticky_color_" + iconobj.color);

			$(_this)
					.children(".textsticky")
					.removeClass(
							function(index, className) {
								return (className
										.match(/\btextsticky_color_\S+/g) || [])
										.join(' ');
							});

			$(_this).children(".textsticky").addClass(
					"textsticky_color_" + iconobj.color);

			$(_this).attr('data-stickycolor', iconobj.color);

		}

	}
	;

	function iconTypeIs(_this) {
		var iconclass = $(_this).attr('class');
		var icontype;

		if (iconclass.indexOf('_tooltip') != -1) {
			icontype = "_tooltip";
		} else if (iconclass.indexOf('_popup') != -1) {
			icontype = "_popup";
		} else if (iconclass.indexOf('_sticky') != -1) {
			icontype = "_sticky";
		}
		;

		return icontype;

	}
	;

	/**
	 * function getPopupColorDetail(iconcolor) { var popupcolorlist = { gray : {
	 * border : "rgba(60, 60, 60, 0.27)", bk : "rgba(136, 136, 136, 0.24)",
	 * aniborder : "rgba(146, 146, 146, 0.36)" }, black : { border : "rgba(36,
	 * 36, 36, 0.27)", bk : "rgba(80, 80, 80, 0.24)", aniborder : "rgba(100,
	 * 100, 100, 0.36)" }, blue : { border : "rgba(20, 70, 150, 0.27)", bk :
	 * "rgba(40, 140, 136, 230.24)", aniborder : "rgba(50, 146, 240, 0.36)" },
	 * white : { border : "rgba(240, 240, 240, 0.27)", bk : "rgba(190, 190, 190,
	 * 0.24)", aniborder : "rgba(180, 180, 180, 0.36)" }, green : { border :
	 * "rgba(60, 60, 60, 0.27)", bk : "rgba(136, 136, 136, 0.24)", aniborder :
	 * "rgba(146, 146, 146, 0.36)" }, red : { border : "rgba(60, 60, 60, 0.27)",
	 * bk : "rgba(136, 136, 136, 0.24)", aniborder : "rgba(146, 146, 146, 0.36)" },
	 * yellow : { border : "rgba(60, 60, 60, 0.27)", bk : "rgba(136, 136, 136,
	 * 0.24)", aniborder : "rgba(146, 146, 146, 0.36)" }, perplue : { border :
	 * "rgba(60, 60, 60, 0.27)", bk : "rgba(136, 136, 136, 0.24)", aniborder :
	 * "rgba(146, 146, 146, 0.36)" }, pink : { border : "rgba(60, 60, 60,
	 * 0.27)", bk : "rgba(136, 136, 136, 0.24)", aniborder : "rgba(146, 146,
	 * 146, 0.36)" } };
	 *
	 * return popupcolorlist[iconcolor]; }
	 */
	function hideModal() {
		$(".modal-overlay").hide();
		$(".slideContentContainer").hide();
		$(".dropped").removeClass("flag_now_edit");
		$('.document').off('dblclick', ".dropped");
		$('.iconStatSave').off('click');

	}
	function isEmail(email) {
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		}

});
