$(function () {
	$('.popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#username',
		modal: true
	});
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		$('.ld-login').show();
		$('.ld-register').hide();
		e.preventDefault();
		$.magnificPopup.close();
	});
});